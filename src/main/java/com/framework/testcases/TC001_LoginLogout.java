package com.framework.testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.Clicklead;
import com.framework.pages.Createleadpage;
import com.framework.pages.Leadspage;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods {
	
	@BeforeTest 
	public void setdata() {
		testCaseName = "TC001_LoginLogout";
		testDescription = "Login into leaftaps";
		testNodes = "Leads";
		author = "Nishanth";
		category = "smoke";
		dataSheetName = "TC001";
		
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCRMFSAbutton();
		 MyHome();
		 Createlead("TL", "Nishp2", "prakasam");
		 
		}
	
	public void MyHome() {
		new Leadspage()
		.clickcreateleadbutton();
	}	
	
	public void Createlead(String TL, String Nishp2, String prakasam) {
		new Createleadpage()
		.entercompanyName(TL)
		.enterfirstName(Nishp2)
		.enterlastName(prakasam)
		.clicksubmitbutton();
	   }	
}
