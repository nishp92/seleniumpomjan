package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class Createleadpage extends ProjectMethods {
	
	public Createleadpage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
		
	}
	
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleentercompanyName; 
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleenterfirstName;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleenterlastName;
	@FindBy(how = How.NAME,using="submitButton") WebElement elesubmitbutton;
	public Createleadpage entercompanyName(String TL) {
		//WebElement eleentercompanyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleentercompanyName, TL);
		return this; 
	}
	
	public Createleadpage enterfirstName(String Nishp2) {
		//WebElement eleenterfirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleenterfirstName, Nishp2);
		return this;
	}
	
	public Createleadpage enterlastName(String prakasam) {
		//WebElement eleenterlastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleenterlastName, prakasam);
		return this;
	}	
	public Viewleadpage clicksubmitbutton() {
		//WebElement elesubmitbutton = locateElement("name", "submitButton");
	    click(elesubmitbutton);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return new Viewleadpage();
	}

}
