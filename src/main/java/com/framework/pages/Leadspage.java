package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class Leadspage extends ProjectMethods {
	
	public Leadspage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
		
	}
	
	@FindBy(how = How.PARTIAL_LINK_TEXT,using="Create Lead") WebElement elecreatelead;
		public Createleadpage clickcreateleadbutton() {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(elecreatelead);
			return new Createleadpage();
			
		}

}
