package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyHome extends ProjectMethods{
	
	public MyHome() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
		
	}
	
	@FindBy(how = How.LINK_TEXT,using="Leads") WebElement eleLeads;
		public Leadspage clickLeadsbutton() {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(eleLeads);
			return new Leadspage();
			

		}
	}
